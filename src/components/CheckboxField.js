import { Field } from 'react-final-form';

export const CheckboxField = ({ name, label }) => {
  return (
    <div className='field field__checkbox'>
      <label>{label}</label>
      <Field name={name} component='input' type='checkbox' />
    </div>
  );
};

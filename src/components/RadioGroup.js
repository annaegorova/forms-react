import { Field } from 'react-final-form';

export const RadioGroup = ({ name, label, options }) => {
  return (
    <div className='field'>
      <label>{label}</label>
      <div className='labels'>
        {options.map(option => {
          return (
            <label key={option.value}>
              <Field
                name={name}
                component='input'
                type='radio'
                value={option.value}
              />
              {option.name}
            </label>
          );
        })}
      </div>
    </div>
  );
};

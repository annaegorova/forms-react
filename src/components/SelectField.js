import { Field } from 'react-final-form';

export const SelectField = ({ name, label, options }) => {
  return (
    <div className='field'>
      <label>{label}</label>
      <Field name={name} component='select'>
        {options.map(option => {
          return option.value
            ? (<option key={option.value} value={option.value}>{option.name}</option>)
            : (<option key={option.value} />)
        })}
      </Field>
    </div>
  );
};

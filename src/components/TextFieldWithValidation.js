import { Field } from 'react-final-form';

export const TextFieldWithValidation = ({ name, label, placeholder, validationFn }) => {
  return (
    <Field name={name} validate={validationFn}>
      {({ input, meta }) => (
        <div className='field'>
          <label>{label}</label>
          <input {...input} type="text" placeholder={placeholder} className={meta.error && meta.dirty ? 'invalid' : ''}/>
        </div>
      )}
    </Field>
  );
};

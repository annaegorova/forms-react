import { Form } from 'react-final-form';
import { TextFieldWithValidation } from '../TextFieldWithValidation';
import { CheckboxField } from '../CheckboxField';
import { SelectField } from '../SelectField';
import { CheckboxesGroup } from '../CheckboxesGroup';
import { RadioGroup } from '../RadioGroup';
import { TextareaField } from  '../TextareaField';
import './CustomForm.css'

const onSubmit = (values) => {
  window.alert(JSON.stringify(values, 0, 2));
};

const validateTextField = (value) => {
  if (value && !(/(^[A-Za-z ]+$)|(^$)/g).test(value)) {
    return 'Invalid value';
  }
};

const validateNumericField = (value) => {
  if (value && !(/(^\d+$)|(^$)/g).test(value)) {
    return 'Invalid value';
  }
};

const validateLength = (value) => {
  if (value?.length > 100) return 'Too long';
};

const colorsOptions = [
  { value: '' },
  { name: 'Red', value: '#ff0000' },
  { name: 'Green', value: '#00ff00' },
  { name: 'Blue', value: '#0000ff' },
];

const sauces = [
  { name: 'Ketchup', value: 'ketchup' },
  { name: 'Mustard', value: 'mustard' },
  { name: 'Mayonnaise', value: 'mayonnaise' },
  { name: 'Guacamole', value: 'guacamole' },
];

const stoogeOptions = [
  { name: 'Larry', value: 'larry' },
  { name: 'Moe', value: 'moe' },
  { name: 'Curly', value: 'curly' },
];

export const CustomForm = () => {
  let formData = {
    stooge: 'larry',
    employed: false
  };

  return (
    <>
      <Form
        onSubmit={onSubmit}
        initialValues={{
          ...formData,
        }}
        render={({ handleSubmit, form, submitting, pristine, values }) => (
          <form onSubmit={handleSubmit} className='form'>
            <TextFieldWithValidation name='firstName' label='First Name' placeholder='First Name' validationFn={validateTextField} />
            <TextFieldWithValidation name='lastName' label='Last Name' placeholder='Last Name' validationFn={validateTextField} />
            <TextFieldWithValidation name='age' label='Age' placeholder='Age' validationFn={validateNumericField} />
            <CheckboxField name='employed' label='Employed'/>
            <SelectField name='favoriteColor' label='Favorite Color' options={colorsOptions}/>
            <CheckboxesGroup name='sauces' label='Sauces' options={sauces} />
            <RadioGroup name='stooge' label='Best Stooge' options={stoogeOptions} />
            <TextareaField name='notes' label='Notes' placeholder='Notes' validationFn={validateLength} />
            <div className='buttons'>
              <button type='submit' disabled={submitting || pristine} className='buttons__submit'>
                Submit
              </button>
              <button
                type='button'
                onClick={form.reset}
                disabled={submitting || pristine}
              >
                Reset
              </button>
            </div>
            <pre>{JSON.stringify(values, 0, 2)}</pre>
          </form>
        )}
      />
    </>
  );
};

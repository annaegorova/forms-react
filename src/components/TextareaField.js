import { Field } from 'react-final-form';

export const TextareaField = ({ name, label, placeholder, validationFn }) => {
  return (
    <Field name={name} validate={validationFn}>
      {({ input, meta }) => (
        <div className='field'>
          <label>{label}</label>
          <textarea {...input} placeholder={placeholder} className={meta.error && meta.dirty ? 'invalid' : ''}/>
        </div>
      )}
    </Field>
  );
}
